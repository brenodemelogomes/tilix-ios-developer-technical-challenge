//
//  ContentViewController.swift
//  Tilix IOS Developer Technical Challenge
//
//  Created by Breno Gomes on 27/02/19.
//  Copyright © 2019 Tilix. All rights reserved.
//

import UIKit
import Kingfisher

class ContentViewController: UIViewController {
    
    var article: Article!
    var articleReadFlag: ArticleReadFlag!
    
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tvContent: UITextView!
    
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbSite: UILabel!
    
    @IBOutlet weak var lbAuthor: UILabel!
    
    @IBOutlet weak var lbTags: UILabel!
    @IBOutlet weak var btChangeReadState: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let imageUrl = URL(string: article.image_url){
            self.ivImage.kf.indicatorType = .activity
            self.ivImage.kf.setImage(with: imageUrl)
            if self.ivImage.image == nil {
                self.ivImage.image = UIImage(named: "default-news-image")
            }
        } else{
            self.ivImage.image = UIImage(named: "default-news-image")
        }
        self.lbTitle.text = article.title
        self.lbDate.text = article.date
        self.lbSite.text = article.website
        self.lbAuthor.text = article.authors
        self.tvContent.text = article.content
        self.tvContent.isEditable = false
        var tags = "Tags: "
        for tag in article.tags{
            tags.append("\(tag.label),")
        }
        tags.remove(at: tags.index(before: tags.endIndex))
        self.lbTags.text = tags
        
        self.lbTitle.adjustsFontSizeToFitWidth = true
        self.lbDate.adjustsFontSizeToFitWidth = true
        self.lbSite.adjustsFontSizeToFitWidth = true
        self.lbAuthor.adjustsFontSizeToFitWidth = true
        
        if articleReadFlag == nil || articleReadFlag.wasRead == false {
            changeReadButtonStyle(read: false)
        } else{
            changeReadButtonStyle(read: true)
        }
    }
    
    func changeReadButtonStyle(read: Bool){
        if read{
            self.btChangeReadState.setTitle("read", for: .normal)
            self.btChangeReadState.backgroundColor = .green
        } else{
            self.btChangeReadState.setTitle("not read", for: .normal)
            self.btChangeReadState.backgroundColor = .red
        }
        
    }
    

    @IBAction func ChangeReadState() {
        if self.articleReadFlag == nil || articleReadFlag.wasRead == false{
            self.articleReadFlag = ArticleReadFlag(context: context)
            self.articleReadFlag.articleName = self.article.title
            self.articleReadFlag.wasRead = true
            changeReadButtonStyle(read: true)
        } else{
            self.articleReadFlag.wasRead = false
            changeReadButtonStyle(read: false)
        }
        do{
            try context.save()
            
        } catch{
            print(error.localizedDescription)
        }
    }
}

