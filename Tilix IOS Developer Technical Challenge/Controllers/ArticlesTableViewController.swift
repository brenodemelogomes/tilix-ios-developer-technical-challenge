//
//  ArticlesTableViewController.swift
//  Tilix IOS Developer Technical Challenge
//
//  Created by Breno Gomes on 27/02/19.
//  Copyright © 2019 Tilix. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class ArticlesTableViewController: UITableViewController {
    var articlesReadFlags:[ArticleReadFlag] = []
    var articleReadFlag: ArticleReadFlag!
    var articles:[Article] = []
    var articlesSearth:[Article] = []
    var article: Article!
    let requestURL = "https://bitbucket.org/!api/2.0/snippets/tilix-dev/AeLkqo/57748e55ada68faa003a283872e32d098c837f0c/files/articles.json"
    var indicator = UIActivityIndicatorView()
    var timeOutOver = false
    let initialDate = Date()
    let maxTimeIntervalForRequest = 10
    let searchController = UISearchController(searchResultsController: nil)
    let minimumArrayLengthToSort = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        inicializateIndicator()
        getData()
        loadArticlesReadFlags()
        DispatchQueue.global(qos: .userInitiated).async {
            self.timeOut()
        }
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barTintColor = .white
        navigationItem.searchController = searchController
        
        searchController.searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.articlesSearth = self.articles
        loadArticlesReadFlags()
        self.tableView.reloadData()
    }
    
    func inicializateIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    func showMessagePopUp(message: String){
        let title = "Atention"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
        
        alert.addAction(okAction)
        present(alert,animated: true, completion: nil)
    }
    
    func seartTitle(searth: String){
        self.articlesSearth = []
        if searth != ""{
            for article in self.articles{
                if article.title.lowercased().contains(searth.lowercased()) {
                    self.articlesSearth.append(article)
                }
            }
        } else{
            self.articlesSearth = self.articles
        }
        tableView.reloadData()
    }
    
    func timeOut(){
        while(!self.timeOutOver){
            let currentDate = Date()
            let calendar = Calendar.current
            let deltaTime = calendar.dateComponents([.second], from: self.initialDate, to: currentDate).second
            if self.articles.isEmpty == false {
                break
            }
            if deltaTime! >= maxTimeIntervalForRequest {
                self.timeOutOver = true
            }
        }
        if self.timeOutOver == true{
            DispatchQueue.main.async {
                let message = "Could not load articles, check your internet connection"
                self.showMessagePopUp(message: message)
            }
        }
    }
    
    func getData(){
        indicator.startAnimating()
        Alamofire.request(self.requestURL).responseJSON { (response) in
            guard let data = response.data else {
                return
            }
            do{
                let articlesList = try JSONDecoder().decode(ArticleList.self, from: data)
                self.articles = articlesList.data
                self.articlesSearth = articlesList.data
                DispatchQueue.main.async {                    
                    self.tableView.reloadData()
                    self.indicator.stopAnimating()
                }
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ContentViewController
        vc.article = article
        vc.articleReadFlag = self.articleReadFlag
        self.articleReadFlag = nil
        self.navigationItem.searchController?.isActive = false
    }
    
    func loadArticlesReadFlags(){
        let fetchRequest: NSFetchRequest<ArticleReadFlag> = ArticleReadFlag.fetchRequest()
        let sortDescritor = NSSortDescriptor(key: "articleName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescritor]
        
        let fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try fetchedResultController.performFetch()
            self.articlesReadFlags = fetchedResultController.fetchedObjects!
            tableView.reloadData()
        } catch {
            fatalError("Failed to fetch entities: \(error.localizedDescription)")
        }
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articlesSearth.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleTableViewCell
        let article = self.articlesSearth[indexPath.row]
        cell.prepare(article: article)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.article = self.articlesSearth[indexPath.row]
        for articleReadFlag in self.articlesReadFlags{
            if articleReadFlag.articleName == self.article.title{
                self.articleReadFlag = articleReadFlag
            }
        }
        performSegue(withIdentifier: "goToContent", sender: nil)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ArticlesTableViewController: UISearchResultsUpdating, UISearchBarDelegate{
    func updateSearchResults(for searchController: UISearchController) {
        //
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        seartTitle(searth: "")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        seartTitle(searth: searchBar.text!)
    }
    
    
}
