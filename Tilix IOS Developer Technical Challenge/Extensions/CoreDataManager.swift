//
//  CoreDataManager.swift
//  Tilix IOS Developer Technical Challenge
//
//  Created by Breno Gomes on 27/02/19.
//  Copyright © 2019 Tilix. All rights reserved.
//

import UIKit
import CoreData

extension UIViewController {
    var context: NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
