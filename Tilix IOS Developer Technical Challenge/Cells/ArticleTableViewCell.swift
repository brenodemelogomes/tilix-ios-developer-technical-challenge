//
//  ArticleTableViewCell.swift
//  Tilix IOS Developer Technical Challenge
//
//  Created by Breno Gomes on 27/02/19.
//  Copyright © 2019 Tilix. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ivImage: UIImageView!
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbSite: UILabel!
    @IBOutlet weak var lbAuthor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepare(article: Article){
        if let imageUrl = URL(string: article.image_url){
            self.ivImage.kf.indicatorType = .activity
            self.ivImage.kf.setImage(with: imageUrl)
            if self.ivImage.image == nil {
                self.ivImage.image = UIImage(named: "default-news-image")
            }
        } else{
            self.ivImage.image = UIImage(named: "default-news-image")
        }
        self.lbTitle.text = article.title
        self.lbDate.text = article.date
        self.lbSite.text = article.website
        self.lbAuthor.text = article.authors
        
        self.lbTitle.adjustsFontSizeToFitWidth = true
        self.lbDate.adjustsFontSizeToFitWidth = true
        self.lbSite.adjustsFontSizeToFitWidth = true
        self.lbAuthor.adjustsFontSizeToFitWidth = true
    }

}
