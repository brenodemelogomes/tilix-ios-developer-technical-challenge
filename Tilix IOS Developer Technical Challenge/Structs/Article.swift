//
//  Article.swift
//  Tilix IOS Developer Technical Challenge
//
//  Created by Breno Gomes on 27/02/19.
//  Copyright © 2019 Tilix. All rights reserved.
//
import Foundation

struct ArticleList: Codable{
    var data: [Article]
}

struct Article: Codable{
    var title: String
    var website: String
    var authors: String
    var date: String
    var content: String
    var tags: [Tag]
    var image_url: String
}

struct Tag: Codable{
    var id: Int
    var label: String
}





